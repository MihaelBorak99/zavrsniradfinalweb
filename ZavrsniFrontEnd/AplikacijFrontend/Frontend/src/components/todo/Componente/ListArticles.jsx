import React,{Component, useState} from 'react'
import ArticleDataService from '../Api/Service/ArticleDataService'
import AuthenticationService from '../Api/Service/AuthenticationService'

class ListArticlesComponent extends Component{
    constructor(props){
        super(props)
        this.state ={
           artikli :  [],
           message : null,
           inputValue : ''
        }
        this.deleteArticleClicked = this.deleteArticleClicked.bind(this)
        this.updateArticleClicked = this.updateArticleClicked.bind(this)
        this.refreshArticles = this.refreshArticles.bind(this)
        this.addArticleClicked = this.addArticleClicked.bind(this)
    }
    componentDidMount(){
        let username = AuthenticationService.getLoggedInUsername()
        this.refreshArticles(username)
    }

    searchArticles(values){
        let username = AuthenticationService.getLoggedInUsername()
        console.log(values.target.value)
        let filtar = values.target.value
        console.log(username)
        ArticleDataService.filtrirajSveArtikle(username,filtar)
        .then(
            response => {
                console.log({artikli : response.data})
                this.setState(() =>{
                    return { artikli:  response.data}
                   }
                )
            }
        )
    }

    deleteArticleClicked(id){
        let username = AuthenticationService.getLoggedInUsername()
        ArticleDataService.obrisiArtikal(username,id)
        .then(
            response => {
                this.setState({message : `Uspješno obrisan artikal sa id-om ${id} `})
                this.refreshArticles(username)
            }
        )
    }

    updateArticleClicked(id){
        this.props.navigate(`/artikli/${id}`)
    }

    addArticleClicked(){
        this.props.navigate(`/artikli/-1`)
    }

    refreshArticles(username){
        ArticleDataService.dohvatiSveArtikle(username)
        .then(
            response => {
                this.setState({artikli : response.data})
            }
        )
    }

    render(){
        return( 
            <div className='naziviTablica'>
                 <h1>Lista Artikla</h1>
                 <div>
                    <input type="text" placeholder="Search..." onChange={this.searchArticles.bind(this)}></input>
                    <button className='btn btn-success dodaj' type='submit'  position-relative onClick={() =>this.addArticleClicked()}>Dodaj novi </button>
                </div>
                <div className='naziviTablica'>
                    <h5 className='nazivi'>Naziv</h5>
                    <h5 className='kategorija'>Kategorija</h5>
                    <h5 className='cijenaa'>Cijena</h5>
                    <h5 className='promijeni'>Promijeni</h5>
                    <h5 className='obrisi'>Obriši</h5>
                </div>
                 {this.state.message && <div className="alert alert-success">{this.state.message}</div>}
                 <div className='container scroll'>
                    <table className='table'>
                        <tbody>
                            {
                            this.state.artikli.map
                                (
                                artikl =>(
                                    <tr key={artikl.id}>
                                        <td>{artikl.naziv}</td>
                                        <td>{artikl.kategorija}</td>
                                        <td>{artikl.cijena + ' ' + 'Kn'}</td>
                                        <td><button className='btn btn-success' onClick={() =>this.updateArticleClicked(artikl.id)}>Promijeni </button></td>
                                        <td><button className='btn btn-warning' onClick={() =>this.deleteArticleClicked(artikl.id)}>Izbriši </button></td>
                                    </tr>
                                )
                                )
                            }
                        </tbody>
                    </table>
                 </div>
            </div> 
        )
    }
}

export default  ListArticlesComponent

