import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import AuthenticationService from '../Api/Service/AuthenticationService'

class HeaderComponent extends Component{
    render(){
        const isUserLoggedIn = AuthenticationService.isUserLoggedIn()
        const getLoggedInUsername = AuthenticationService.getLoggedInUsername()
        return (
            <header>
                <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                    <ul className="navbar-nav">
                        {isUserLoggedIn &&<li ><Link className="nav-link" to ="/stolovi">Stol</Link></li>}
                        {isUserLoggedIn &&<li ><Link className="nav-link" to ="/narudzba">Kreiraj narudzbu</Link></li>}
                        {isUserLoggedIn && <li ><Link className="nav-link" to ="/artikli">Artikli</Link></li>}
                        {isUserLoggedIn && <li ><Link className="nav-link" to ="/dodajKorisnika">Dodaj korisnika</Link></li>}
                    </ul>

                    <ul className="navbar-nav navbar-collapse justify-content-end">
                        {isUserLoggedIn &&<li className='nav-link'>{getLoggedInUsername}</li>}
                        {!isUserLoggedIn &&<li><Link className="nav-link" to ="/login">Login</Link></li>}
                        {isUserLoggedIn && <li><Link className="nav-link" to ="/logout" onClick={AuthenticationService.logout}>Logout</Link></li>}
                    </ul>
                </nav>
            </header>
        
        )
    }


}
export default HeaderComponent