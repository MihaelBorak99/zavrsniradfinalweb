
import React,{Component, useState} from 'react'
import Popup from '../Functions/Popup'
import StolDataService from '../Api/Service/StolDataService'
import AuthenticationService from '../Api/Service/AuthenticationService'

class StolComponent extends Component{
    constructor(props){
        super(props)
        this.state ={
           buttonPopupSlobodan:false,
           buttonPopupZauzet: false,
           trenutniStol : 0,
        }
        this.rezervirajStol = this.rezervirajStol.bind(this)
        this.potvrdiRezervaciju = this.potvrdiRezervaciju.bind(this)
        this.otkaziRezervaciju = this.otkaziRezervaciju.bind(this)
        this.kreirajRacun = this.kreirajRacun.bind(this)
    }

    potvrdiRezervaciju(){
        let id = this.state.trenutniStol
        let username = AuthenticationService.getLoggedInUsername()
        StolDataService.rezervirajStol(username,id)
        this.setState({buttonPopupSlobodan : false})
        document.getElementById(this.state.trenutniStol).style.background = "red"

    }


    rezervirajStol(id){
        let username = AuthenticationService.getLoggedInUsername()
        console.log("v rezerviraj stolu sam")
        StolDataService.dohvatiStol(username,id)
        .then(
            response => {
                console.log("Response dobiveni je:" + response.data.slobodan)
                let stanje = response.data.slobodan
                this.otvoriPopup(id,stanje)
            }
        )
    }

    otvoriPopup(id,stanje){
        console.log("v otvori popup sam")
        if(stanje === true){
            console.log("v ifu sam " + stanje)
            this.setState({buttonPopupSlobodan : true})
            this.setState({trenutniStol : id})
            }
            if(stanje === false){
                console.log("v drugom ifu sam sam" + stanje)
                this.setState({buttonPopupZauzet : true})
                this.setState({trenutniStol : id})
            }
    }
    kreirajRacun(){
        this.props.navigate(`/narudzba`)
    }
    otkaziRezervaciju(){
        let id = this.state.trenutniStol
        let username = AuthenticationService.getLoggedInUsername()
        StolDataService.otkaziRezervaciju(username,id)
        this.setState({buttonPopupZauzet : false})
        document.getElementById(this.state.trenutniStol).style.background = "green"
    }


    render(){
        return(

            <div>
                    <Popup trigger={this.state.buttonPopupSlobodan}>
                        <h3>Stol: {this.state.trenutniStol}</h3>
                        <button className='btn btn-danger' onClick={() => this.potvrdiRezervaciju()}>rezerviraj</button>
                        
                    </Popup>

                    <Popup trigger={this.state.buttonPopupZauzet}>

                        <h3>Stol: {this.state.trenutniStol}</h3>
                        <button className='btn btn-success zavrsi'   onClick={() => this.otkaziRezervaciju()}>otkazi rezervaciju</button>
                        <button className='btn btn-info otkazi'  onClick={() => this.kreirajRacun()}>kreiraj racun</button>
                        
                    </Popup>


                <div className='poljeGumbova'>
                <button id='1' className='rounded-circle btn btn-success btn-lg' onClick={() => this.rezervirajStol(1)}>Stol1</button>
                <button id='2'className='rounded-circle btn btn-success btn-lg' onClick={() => this.rezervirajStol(2)}>Stol2</button>
                <button id='3'className='rounded-circle btn btn-success btn-lg' onClick={() => this.rezervirajStol(3)}>Stol3</button>
                </div>
                <div className='poljeGumbova'>
                <button id='4'className='rounded-circle btn btn-success btn-lg' onClick={() => this.rezervirajStol(4)}>Stol4</button>
                <button id='5'className='rounded-circle btn btn-success btn-lg' onClick={() => this.rezervirajStol(5)}>Stol5</button>
                <button id='6'className='rounded-circle btn btn-success btn-lg' onClick={() => this.rezervirajStol(6)}>Stol6</button>
                </div>
                <div className='poljeGumbova'>
                <button id='7'className='rounded-circle btn btn-success btn-lg' onClick={() => this.rezervirajStol(7)}>Stol7</button>
                <button id='8'className='rounded-circle btn btn-success btn-lg' onClick={() => this.rezervirajStol(8)}>Stol8</button>
                <button id='9'className='rounded-circle btn btn-success btn-lg' onClick={() => this.rezervirajStol(9)}>Stol9</button>
                </div>
            </div>)
    }
}

export default StolComponent