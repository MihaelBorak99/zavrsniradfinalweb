import React,{Component} from 'react'
import { Navigate } from 'react-router-dom'
import AuthenticationService from '../Api/Service/AuthenticationService.js'

class AdminRoute extends Component{
    render(){
        const username = AuthenticationService.getLoggedInUsername()
        const isUserAdmin = AuthenticationService.isUserAdmin(username)
        console.log(isUserAdmin)
        if(isUserAdmin){
            return {...this.props.children}
        }
        else{
            console.log("nije admin ")
            return <Navigate to ="/stolovi" />
        }

    }
}

export default AdminRoute