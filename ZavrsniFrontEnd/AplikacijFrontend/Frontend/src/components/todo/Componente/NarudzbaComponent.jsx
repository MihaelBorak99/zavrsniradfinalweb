import React,{Component, useState} from 'react'
import ArticleDataService from '../Api/Service/ArticleDataService'
import AuthenticationService from '../Api/Service/AuthenticationService'
import {List, ListItem, ListItemText } from '@mui/material'
import Popup from '../Functions/Popup'
import NarudzbaDataService from '../Api/Service/NarudzbaDataService'

class NarudzbaComponent extends Component{
    constructor(props){
        var vreme = new Date()
        var date = vreme.getDate() + '/' + (vreme.getMonth()+1) + '/' + vreme.getFullYear();
        var time = vreme.getHours() + ':' + vreme.getMinutes() + ':' + vreme.getSeconds();
        super(props)
        this.state ={
           currentDate: date,
           currentTime: time,
           inputValue : '',
           artikli:[],
           narudzba: [],
           kolicina: 1,
           ukupnaCijena: 0,
           narudzbaArtikli : [],
           buttonPopup:false,
           nacinPlacanja : 'gotovina',
           idNarudzbeZaObrisat: 0,

        }
        this.refreshArticles = this.refreshArticles.bind(this)
        this.refreshNarudzbaArticles = this.refreshNarudzbaArticles.bind(this)
        this.kreirajNarudzbu = this.kreirajNarudzbu.bind(this)
        this.obrisiNarudzbu  = this.obrisiNarudzbu.bind(this)
    }

    componentDidMount(){
        let username = AuthenticationService.getLoggedInUsername()
        console.log(username)
        this.refreshArticles(username)

    }

    
    refreshNarudzbaArticles(id){
        let username = AuthenticationService.getLoggedInUsername()
        ArticleDataService.dohvatiArtikal(username, id).then
        (
            response => {
                let nar = this.state.narudzbaArtikli
                response.data["kolicina"] = this.state.kolicina
                let dodaniArtikl = response.data
                let isFound = nar.some(artikl => {
                    if(artikl.id === dodaniArtikl.id){
                        return true;
                    }
                    return false;
                });
                if(isFound){
                    nar.forEach(artikl => {
                        if(artikl.id === dodaniArtikl.id){
                            artikl.kolicina +=1
                        }
                    });   
                }
                else{
                    nar.push(response.data);
                }
                this.setState({narudzbaArtikli : nar})
            }
        )
        this.refreshArticles(username)
    }

    //dodavanje kolicine
    dodaj(artikl){
        let username = AuthenticationService.getLoggedInUsername()
        artikl.kolicina = (artikl.kolicina + 1)
        this.refreshArticles(username)
      }
    
      //Oduzimanje kolicine artikli
    oduzmi(artikl){
        let username = AuthenticationService.getLoggedInUsername()
        if(artikl.kolicina > 1){
        artikl.kolicina = (artikl.kolicina - 1)
        this.refreshArticles(username)
        }
      }
      
      //Izaz is popup ako je sve okej
      zavrsiNarudzbu(){
        this.setState({buttonPopup : false})
        this.setState({narudzbaArtikli : []})
        console.log(this.state.narudzbaArtikli)

      }
    refreshArticles(username){
        ArticleDataService.dohvatiSveArtikle(username)
        .then(
            response => {
                this.setState({artikli : response.data})
            }
        )
    }
    kreirajNarudzbu(){
        this.setState({buttonPopup : true})
        let username = AuthenticationService.getLoggedInUsername()
        let naruceniArtikli = this.state.narudzbaArtikli
        NarudzbaDataService.kreirajNarudzbu(username,naruceniArtikli)
        .then(
            response => {
                console.log({narudzba : response.data})
                this.setState(() =>{
                    return { narudzba:  response.data}
                   }
                )
            }
        )
    }

    obrisiNarudzbu(){
        this.setState({buttonPopup : false})
        let username = AuthenticationService.getLoggedInUsername()
        NarudzbaDataService.obrisiNarudzbu(username)
    }
    searchArticles(values){
        let username = AuthenticationService.getLoggedInUsername()
        console.log(values.target.value)
        let filtar = values.target.value
        console.log(username)
        ArticleDataService.filtrirajSveArtikle(username,filtar)
        .then(
            response => {
                console.log({artikli : response.data})
                this.setState(() =>{
                    return { artikli:  response.data}
                   }
                )
            }
        )
    }
    render(){
        return( 
            <div className='content'>
            <div className='leftSide'>

                 <div className='searchBox'>
                    <input type="text" placeholder="Search..." onChange={this.searchArticles.bind(this)}></input>
                    </div>


                    <div className='scroll'>
                        <List className='list'>
                            {
                            this.state.artikli.map
                                (
                                artikl =>(
                                    <ListItem key={artikl.id}>
                                        <ListItemText className='naziv'>{'-'+ ' '+artikl.naziv}</ListItemText>
                                        <ListItemText className='cijena'>{'Kn'+artikl.cijena}</ListItemText>
                                        <ListItemText><button type="button" class='btn btn-primary  btn-xs btn-dodaj' onClick={() =>this.refreshNarudzbaArticles(artikl.id)}>Dodaj</button></ListItemText>
                                    </ListItem>
                                )
                                )
                            }
                        </List>
                         </div>
                </div>
                    <div className='container rightSide'>
                    <table className='table'>
                        <thead>
                            <tr>
                                <th>Artikl</th>
                                <th>Kolicina</th>
                                <th>Cijena</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                            this.state.narudzbaArtikli.map
                                (
                                artikl =>(
                                    <tr key={artikl.id}>
                                        <td>{artikl.naziv}</td>
                                        <td>{artikl.kolicina + 'x'}</td>
                                        <td>{artikl.cijena + ' ' + 'Kn'}</td>
                                        <td><button className='btn btn-success' onClick={() =>this.dodaj(artikl)}>+1 </button></td>
                                        <td><button className='btn btn-danger' onClick={() =>this.oduzmi(artikl)}>-1 </button></td>
                                    </tr>
                                )
                                )
                            }
                        </tbody>
                    </table>
                    </div>
                 <div> 
                    <Popup trigger={this.state.buttonPopup}>
                        <h3>Račun</h3>
                        <List className='list'>
                            {
                            this.state.narudzbaArtikli.map
                                (
                                artikl =>(
                                    <ListItem key={artikl.id}>
                                        <ListItemText className='kolicina'>{artikl.kolicina + 'x'} </ListItemText>
                                        <ListItemText className='naziv' >{artikl.naziv}</ListItemText>
                                        <ListItemText className='cijena'>{artikl.cijena +' Kn'}</ListItemText>
                                       
                                    </ListItem>
                                )
                                )
                            }
                            
                            <hr></hr>
                            <ListItem>
                            <ListItemText>{'Blagajnik: ' + this.state.narudzba.id_Korisnika}</ListItemText>
                            </ListItem>
                            <ListItem>
                            <ListItemText>{'Nacin placanja: ' + (this.state.nacinPlacanja)}</ListItemText>
                            <ListItemText className='listRight'>{'Osnovica: ' + (this.state.narudzba.ukupanIznos - this.state.narudzba.ukupanPorez +'Kn')}</ListItemText>
                            </ListItem>
                            <ListItem>
                            <ListItemText>{'Datum: ' + this.state.currentDate}</ListItemText>
                            <ListItemText className='listRight'>{'Ukupan porez: ' + this.state.narudzba.ukupanPorez +' Kn'}</ListItemText>
                            </ListItem>
                            <ListItem>
                            <ListItemText>{'Vrijeme: ' + this.state.currentTime}</ListItemText>
                            <ListItemText className='listRight'>{'Ukupno za platiti: ' + this.state.narudzba.ukupanIznos +' Kn'}</ListItemText>
                            </ListItem>
                        </List>

                        
                    
                        <button className='btn btn-success zavrsi' onClick={() => this.zavrsiNarudzbu()}>završi</button>
                        <button className='btn btn-danger otkazi' onClick={() => this.obrisiNarudzbu()}>otkaži</button>
                        
                    </Popup>
                 <button className='btn btn-info naplati' onClick={() => this.kreirajNarudzbu()}>Naplati</button>
                 <div className='radio'>
                    <input type="radio" value="kartica" name="nacinPlacanja" onClick={()=> this.setState({nacinPlacanja : 'kartica'})}/> Kartica &nbsp;&nbsp;&nbsp;
                    <input type="radio" defaultChecked  value="gotovina" name="nacinPlacanja" onClick={()=> this.setState({nacinPlacanja : 'gotovina'})}/> Gotovina
                </div>
                </div>

              </div>
        )
    }        

}

export default NarudzbaComponent