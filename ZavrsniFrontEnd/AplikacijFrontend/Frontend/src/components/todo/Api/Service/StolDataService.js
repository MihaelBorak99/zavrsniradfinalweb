import axios from "axios"

class StolDataService{

    rezervirajStol(name,id){
        return axios.post(`http://localhost:9192/korisnici/${name}/stolovi/${id}`)
    }
    otkaziRezervaciju(name,id){
        return axios.put(`http://localhost:9192/korisnici/${name}/stolovi/${id}`)
    }
    dohvatiStol(name,id){
        return axios.get(`http://localhost:9192/korisnici/${name}/stolovi/${id}`)
    }
}

export default new StolDataService()