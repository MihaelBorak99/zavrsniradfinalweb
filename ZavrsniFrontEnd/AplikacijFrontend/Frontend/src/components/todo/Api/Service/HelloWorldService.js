import axios from "axios"

class HelloWorldService{

    executeHelloWorldBeanService(){
        return axios.get("http://localhost:8080/hello-world-bean")
    }

    executeHelloWorldService(){
        return axios.get("http://localhost:8080/hello-world")
    }

    executeHelloWorldPathVariableService(name){
        return axios.get(`http://localhost:8080/hello-world/path-variable/${name}`)
        
    }

    executeBookWorldService(){
        return axios.get("http://localhost:8080/book-world/books")
    }
}

export default new HelloWorldService()