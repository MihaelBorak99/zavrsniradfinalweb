import axios from "axios"

class ArticleDataService{
    dohvatiSveArtikle(name){
        return axios.get(`http://localhost:9192/korisnici/${name}/artikli`)
    }
    filtrirajSveArtikle(name,filtar){
        return axios.get(`http://localhost:9192/korisnici/${name}/artikli/`, {params: {filtar:filtar}})
    }
    dohvatiArtikal(name,id){
        return axios.get(`http://localhost:9192/korisnici/${name}/artikli/${id}`)
    }
    obrisiArtikal(name,id){
        return axios.delete(`http://localhost:9192/korisnici/${name}/artikli/${id}`)
    }
    promijeniArtikal(name,id,artikal){
        return axios.put(`http://localhost:9192/korisnici/${name}/artikli/${id}`, artikal)
    }
    dodajNoviArtikal(name,artikal){
        return axios.post(`http://localhost:9192/korisnici/${name}/artikli/`, artikal)
    }
}

export default new ArticleDataService()