import axios from "axios"
class AuthenticationService{


    executeBasicAutenhiticationService(username, password){
        return axios.get('http://localhost:9192/basicAuth', 
        {headers: {authorization: this.createBasicAuthToken(username,password)}})
    }

    createBasicAuthToken(username,password){
        return 'Basic ' + window.btoa(username + ":" + password)
    }

    
    registerSuccessfulLogin(username,password){

        sessionStorage.setItem('authenticatedUser', username)
        this.setupAxiosInterceptors(this.createBasicAuthToken(username,password))
    }

    logout(){
        sessionStorage.removeItem('authenticatedUser')
    }

    isUserAdmin(name){
        axios.get(`http://localhost:9192/korisnici/${name}/dohvatiAdmina`)
    }
    isUserLoggedIn(){
        let user = sessionStorage.getItem('authenticatedUser')
        if(user===null) return false
        return true
    }
    getLoggedInUsername(){
        let user = sessionStorage.getItem('authenticatedUser')
        if(user===null) return '';
        return user;
    }

    setupAxiosInterceptors(basicAuthHeader){

        axios.interceptors.request.use(
            (config) => {
                if(this.isUserLoggedIn()){
                    config.headers.authorization = basicAuthHeader
                }
                return config
            }
        )
    }
}

export default new AuthenticationService()