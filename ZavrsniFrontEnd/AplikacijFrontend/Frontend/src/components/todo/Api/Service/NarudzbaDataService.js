import axios from "axios"

class ArticleDataService{

    kreirajNarudzbu(name,artikal){
        return axios.post(`http://localhost:9192/korisnici/${name}/narudzba`, artikal)
    }
    obrisiNarudzbu(name){
        return axios.delete(`http://localhost:9192/korisnici/${name}/narudzba`)
    }
}

export default new ArticleDataService()