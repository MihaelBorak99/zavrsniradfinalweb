import React, { Component } from 'react'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import withNavigation from './Functions/WithNavigation.jsx'
import withParams from './Functions/WithParams.jsx'
import AuthenticatedRoute from './Componente/AuthenticatedRoute.jsx'
import LoginComponent from './Componente/LoginComponent.jsx'
import ListTodosComponent from './Componente/ListArticles.jsx'
import HeaderComponent from './Componente/HeaderComponent.jsx'
import FooterComponent from './Componente/FooterComponent.jsx'
import LogoutComponent from './Componente/LogoutComponent.jsx'
import WelcomeComponent from './Componente/WelcomeComponent.jsx'
import ErrorComponent from './Componente/ErrorComponent.jsx'
import ArticleComponent from './Componente/ArticleComponent.jsx'
import NarudzbaComponent from './Componente/NarudzbaComponent.jsx'
import StolComponent from './Componente/Stol.jsx'
import DodajKorniskaComponent from './Componente/DodajKorisnika.jsx'
import AdminRoute from './Componente/AdminRoute.jsx'
class RestoranApp extends Component{
    render(){
        
        const LoginComponentWithNavigation = withNavigation(LoginComponent)
        const WelcomeComponentWithParams = withParams(WelcomeComponent)
        const HeaderComponentWithNavigation = withNavigation(HeaderComponent)
        const ListTodosComponentWithNavigation = withNavigation(ListTodosComponent)
        const ArticleComponentWithParamsAndNavigation = withParams(withNavigation(ArticleComponent))
        const NarudzbaComponentWithParamsAndNavigation = withParams(withNavigation(NarudzbaComponent))
        const StolComponentWithParamsAndNavigation = withParams(withNavigation(StolComponent))
        const DodajKorisnikaWithParamsAndNavigation = withParams(withNavigation(DodajKorniskaComponent))

        return(
            <div className='TodoApp'>
                <Router>
                <HeaderComponentWithNavigation/>
                    <Routes>
                    <Route path="/" element={<LoginComponentWithNavigation />} />
                    <Route path = "/login" element={<LoginComponentWithNavigation />}/>
                    <Route path = "/welcome/:name" element={<AuthenticatedRoute><WelcomeComponentWithParams /></AuthenticatedRoute>}/>
                    <Route path = "/artikli" element={<AuthenticatedRoute><ListTodosComponentWithNavigation /></AuthenticatedRoute>}/>
                    <Route path = "/narudzba" element={<AuthenticatedRoute><NarudzbaComponentWithParamsAndNavigation /></AuthenticatedRoute>}/>
                    <Route path = "/artikli/:id" element={<AuthenticatedRoute><ArticleComponentWithParamsAndNavigation /></AuthenticatedRoute>}/>
                    <Route path ="/logout" element={<AuthenticatedRoute><LogoutComponent/></AuthenticatedRoute>}/>
                    <Route path = "/dodajKorisnika" element ={<AuthenticatedRoute><DodajKorisnikaWithParamsAndNavigation/></AuthenticatedRoute>}/>
                    <Route path="*" element={<ErrorComponent />}/>
                    <Route path= "/stolovi" element={<AuthenticatedRoute><StolComponentWithParamsAndNavigation /></AuthenticatedRoute>}/>
                   </Routes>
                   <FooterComponent/>
                </Router>
            </div>
        )
    }
}

export default RestoranApp