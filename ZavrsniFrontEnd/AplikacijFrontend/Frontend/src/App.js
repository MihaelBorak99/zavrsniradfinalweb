import React, { Component } from 'react';
import RestoranApp from './components/todo/RestoranApp'
import './App.css'
import './bootstrap.css'
 
class App extends Component {
  render() {
    return (
      <div className="App">
      {/* <Counter/>*/}
      <RestoranApp/>
      </div>
    );
  }
}
export default App;